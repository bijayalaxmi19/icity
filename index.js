/**
 * @format
 */
import {AppRegistry} from 'react-native';
import Index from './js/index.js';
import App from './App';
import {name as appName} from './app.json';
AppRegistry.registerComponent(appName, () => Index);

