import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer } from 'react-navigation';
import React, { Component } from 'react';
import { Platform,Dimensions } from 'react-native';
import Firstpage from '../screen/Firstpage';
import Otppage from '../screen/Otppage';
import { create } from 'istanbul-reports';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

// const NavigationMenu = DrawerNavigator(
//   Drawer,
//   {
//     initialRouteName:'Splash',
//     drawerWidth: screenWidth - (Platform.OS === 'android' ? 56 : (screenWidth >414 ? 500 : 64)),
//     contentComponent: props => <SideBar {...props} routes={Drawer}/>,
//     drawerPosition:'left',
//   });

  const StackNav = createStackNavigator(
    {
     Firstpage:{screen:Firstpage},
     Otppage:{screen:Otppage},
    },
  {headerMode:'none'});
  
  const Stacknavigation = createAppContainer(StackNav);
  export default Stacknavigation;
