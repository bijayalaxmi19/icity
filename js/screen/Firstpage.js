
import React,{Component} from 'react';
import {SafeAreaView,StyleSheet,ScrollView,View,Text,TouchableOpacity,StatusBar,Dimensions,Image} from 'react-native';
import { Container,Content,Input,Card, Header, Left, Body, Right, Title, Subtitle,Icon,Footer,FooterTab } from 'native-base';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default class Firstpage extends Component{
  state={entries:["1","2","3","4","5","6","7","8","9","10"]}
  _renderItem ({item, index}, parallaxProps) {
    return (
    <View style={styles.item}>
    <ParallaxImage
     source={require('../assets/image.jpeg')}
     containerStyle={styles.imageContainer}
     style={styles.image} parallaxFactor={0.3} {...parallaxProps}/>
     <Text style={styles.title} numberOfLines={2}>
      { item.title }
     </Text>
      </View>
    );
}

    render(){
      return(
        <Container>
        <View style={{backgroundColor:'#fff',flexDirection:'row'}} >
        <View style={{flexDirection:'column',marginTop:8,marginLeft:8}}>  
        <Text style={{color:'#000'}}>Hello</Text>
        <Text style={{color:'#000',fontSize:15,fontWeight:'bold'}}>Good Evening</Text>
        </View>
        <Image source={require('../assets/sun.jpg')} style={{width:60,height:60,marginLeft:'60%'}}/>
        </View> 

       <Content>
         <View style={{alignItems:'center',alignContent:'center',justifyContent:'center'}}>
         <View style={{width:screenWidth,flexDirection:'row',justifyContent:'center'}}>
           <View style={{alignItems:'center',alignContent:'center',justifyContent:'center'}}>
           <Card style={{width:60,height:60,borderRadius:50,backgroundColor:'#EE8B11',alignContent:'center',alignItems:'center',justifyContent:'center'}}>
           <Icon name="ios-add-circle" style={{color:'#fff'}}></Icon>
           </Card>
           <Text>RM</Text>
           <Text>250.00</Text>
           </View>
           <View style={{borderLeftColor:'#C0C0C0',borderLeftWidth:1,height:screenWidth/3,marginTop:50,marginLeft:50,marginRight:50}}/>
           <View style={{alignItems:'center',alignContent:'center',justifyContent:'center'}}>
           <Card style={{width:60,height:60,borderRadius:50,backgroundColor:'#EE8B11',alignContent:'center',alignItems:'center',justifyContent:'center'}}>
           <Icon name="ios-add-circle" style={{color:'#fff'}}></Icon>
           </Card>
           <Text>POINTS</Text>
           <Text>450</Text>
           </View>
    
         </View>  
         <Image source={require('../assets/qrcode.png')} style={{width:200,height:200}}/>
         <TouchableOpacity style={{width:200,height:40,backgroundColor:'#fff',elevation:1,borderRadius:10,borderColor:'#EE8B11',borderWidth:0.5,
        justifyContent:'center',alignItems:'center'}} onPress={()=>this.props.navigation.navigate("Otppage")}>
          <Text style={{color:'#EE8B11',fontSize:20}}>Scan</Text>
         </TouchableOpacity>
         </View>
         <View style={{marginTop:15}}>
         <Carousel sliderWidth={screenWidth} sliderHeight={screenWidth} itemWidth={screenWidth - 60} data={this.state.entries} renderItem={this._renderItem}
            hasParallaxImages={true} autoplay={true} loop={true}/>
            </View>
        </Content>

        <Footer>
            <FooterTab style={{ backgroundColor: '#fff',alignItems:'center',paddingLeft:5,paddingRight:5 }}>
              <TouchableOpacity style={{width:100,height:30,backgroundColor:'#ECC89C',borderRadius:10,alignItems:'center',alignContent:'center',justifyContent:'center'}}>
                <Text style={{color:'#EE8B11'}}>Home</Text>
              </TouchableOpacity>  
             <Icon name='ios-mail'/>
             <Icon name="ios-cog"/>
             <Icon name="ios-cog"/>
            </FooterTab>
          </Footer>


       </Container>
        )
    }
}
const styles = StyleSheet.create({
  item: {
    width: screenWidth - 60,
    height:150,
  },
  imageContainer: {
    flex: 1,
    marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
    backgroundColor: 'white',
    borderRadius: 8,
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: 'cover',
  },
})



