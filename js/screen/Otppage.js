
import React,{Component} from 'react';
import {SafeAreaView,StyleSheet,TextInput,Dimensions,View,Text,StatusBar,TouchableOpacity} from 'react-native';
import { Container,Content,Input,Item,Card, Header, Left, Body, Right, Title, Subtitle,Icon } from 'native-base';
import { Grid, Col } from 'react-native-easy-grid';
import CountDown from 'react-native-countdown-component';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
class Otppage extends React.Component{
  state={otp:[],timer:60};
  otpTextInput = [];
  
  componentDidMount() {
    this.otpTextInput[0]._root.focus();
   }
   
renderInputs() {
  const inputs = Array(4).fill(0);
  const txt = inputs.map(
      (i, j) => <Col key={j} ><Item regular style={{elevation:1,margin:7}}>
          <Input
              style={{textAlign: 'center',borderRadius: 10 }}
              keyboardType="numeric"
              onChangeText={v => this.focusNext(j, v)}
              onKeyPress={e => this.focusPrevious(e.nativeEvent.key, j)}
              ref={ref => this.otpTextInput[j] = ref}
          />
      </Item></Col>
  );
  return txt;
}

focusPrevious(key, index) {
  if (key === 'Backspace' && index !== 0)
      this.otpTextInput[index - 1]._root.focus();
}

focusNext(index, value) {
  if (index < this.otpTextInput.length - 1 && value) {
      this.otpTextInput[index + 1]._root.focus();
  }
  if (index === this.otpTextInput.length - 1) {
      this.otpTextInput[index]._root.blur();
  }
  const otp = this.state.otp;
  otp[index] = value;
  this.setState({ otp });
  
}


    render(){
      return(
         <Container>
        
          <View style={{backgroundColor:'#fff',flexDirection:'row'}} >
        <View style={{flexDirection:'column',marginTop:8,marginLeft:8}}>  
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Firstpage")}>
           <Icon name='arrow-back' style={{color:'#EE8B11'}}/>
          </TouchableOpacity> 
        </View>
        <Text style={{fontSize:25,color:'#EE8B11',paddingLeft:'28%',marginTop:9}}>Verifications </Text>   
       </View> 
          <Content>
            <View style={{alignContent:'center',alignItems:'center',marginTop:'25%',margin:'15%'}}>
              <Text style={{fontSize:20}}>Please enter the code from the phone</Text>
              <Text style={{fontSize:20}}>number we just sent</Text>
              
              <Grid style={{paddingTop:30}}>
                    {this.renderInputs()}
                </Grid>

              <TouchableOpacity style={{width:screenWidth-100,height:'20%',borderRadius:30,backgroundColor:'#EE8B11',marginTop:'20%',alignItems:'center',alignContent:'center',justifyContent:'center',
            elevation: 2,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}>
              <Text style={{fontSize:20,color:'#fff'}}>Submit Now</Text>
               </TouchableOpacity> 
               <Text style={{fontSize:15,paddingTop:10}}>I didn't get a code</Text>
               <View style={{flexDirection:'row'}}>
               <Text style={{fontSize:15,color:'#EE8B11',padding:5}}>Resend code </Text>
               <CountDown
        until={60}
        size={10}
        digitStyle={{backgroundColor: '#FFF'}}
        digitTxtStyle={{color: '#EE8B11',fontSize:15}}
        timeToShow={['M', 'S']}
        timeLabelStyle={{color:'#fff'}}
        showSeparator={false}
        separatorStyle={{color:'#EE8B11',marginBottom:8}}
        
      />
      </View>
              </View> 
             

          </Content>
             
          </Container>   
        )
    }
}
export default Otppage;







{/* <Header style={{backgroundColor:'#fff'}} >
<Left style={{flexDirection:'row'}}>
<TouchableOpacity >
<Icon name='arrow-back' style={{color:'#EE8B11'}}/>
</TouchableOpacity>  
</Left>
<Body style={{paddingLeft:'25%'}}>
<Text style={{fontSize:20,color:'#EE8B11'}}>Verifications </Text>   
</Body>
<Right></Right>
</Header>
<Content>
 <View style={{alignContent:'center',alignItems:'center',margin:'15%'}}>
   <Text style={{fontSize:20}}>Please enter the code from the phone</Text>
   <Text style={{fontSize:20}}>number we just sent</Text>
   
   <View style={{flexDirection:'row',paddingTop:20}}>
   <Card style={{width:40,height:40}}>
   <TextInput keyboardType='numeric'maxLength={1}></TextInput>
   </Card> 
   <Card style={{width:40,height:40,marginLeft:5}}>
   <TextInput keyboardType='numeric'maxLength={1}></TextInput>
   </Card> 
   <Card style={{width:40,height:40,marginLeft:5}}>
   <TextInput keyboardType='numeric'maxLength={1}></TextInput>
   </Card> 
   <Card style={{width:40,height:40,marginLeft:5}}>
   <TextInput keyboardType='numeric'maxLength={1}></TextInput>
   </Card> 
   </View>
   <TouchableOpacity style={{width:screenWidth-100,height:'20%',borderRadius:30,backgroundColor:'#EE8B11',marginTop:'20%',alignItems:'center',alignContent:'center',justifyContent:'center',
 elevation: 2,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}>
   <Text style={{fontSize:20,color:'#fff'}}>Submit Now</Text>
    </TouchableOpacity> 
    <Text style={{fontSize:15,paddingTop:10}}>I didn't get a code</Text>
    <Text style={{fontSize:15,color:'#EE8B11',padding:5}}>Resend code 00:32</Text>
   </View>  */}